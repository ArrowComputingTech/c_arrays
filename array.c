#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct CA { // CA = Configurable Array
  int *arr;
  int capacity;
  int used;
};
int value;

int get(struct CA, int indx);
void set(struct CA, int indx, int value);
struct CA resize(struct CA arr);
struct CA append(struct CA arr, int value);
struct CA trim_to_size(struct CA arr);
int createArr(int *arr, int capcity);
int printArr();

int main() {
  struct CA array1;
  array1.arr = malloc(sizeof(int));
  array1.capacity = 4; // sizeof(array1.arr);
  array1.used = createArr(array1.arr, array1.capacity);

  int menuSelection = 1;
  int menuSize = 7;
  int indx = 0;

  while (menuSelection != menuSize) {
    if (menuSelection < 1 || menuSelection > menuSize) {
      printf("Invalid selection.\n");
    } else {
        switch (menuSelection) {
          case 1:
            printf("Current array: \n");
            for (int i = 0; i < array1.used; i++) {
              printf("%d ", array1.arr[i]);
            }
            printf("\n");
            printf("Array has size of %d.\n", array1.capacity);
            break;

          case 2: // get()
            printf("Which element would you like to get?\n");
            printf("(0-%d): ", (array1.used - 1));
            scanf("%d", &indx);
            value = get(array1, indx);
            printf("Element %d: %d\n", indx, value);
            break;

          case 3: // set()
            printf("Which element would you like to set?\n");
            printf("(0-%d): ", (array1.used - 1));
            scanf("%d", &indx);
            printf("What would you like to use as the new value?\n");
            scanf("%d", &value);
            set(array1, indx, value);
            break;

          case 4:
            printf("Resizing array...");
            array1 = resize(array1);
            break;

          case 5:
            printf("Enter a new value to add to array: ");
            scanf("%d", &value);
            array1 = append(array1, value);
            break;

          case 6:
            printf("Trimming array to size...\n");
            array1 = trim_to_size(array1);
            break;

          case 7:
            return 0;

          default:
            printf("That's probably an invalid selection...\n");
            break;
        }
      }
    printf("What would you like to do?\n");
    printf("1. Print array.\n");
    printf("2. Get an element.\n");
    printf("3. Set an element.\n");
    printf("4. Reisze array..\n");
    printf("5. Append a value to array.\n");
    printf("6. Trim array to used size.\n");
    printf("7. Exit.\n");
    scanf("%d", &menuSelection);
  }
}

int createArr(int *arr, int capacity) {
  int used;
  for (int i = 0; i < capacity; i++) {
    printf("Enter an integer value: ");
    scanf("%d", &value);
    *(arr + i) = value;
    used = i;
  }
  return (used + 1);
}

int get(struct CA arr, int indx) {
  int returnVal;
    returnVal = arr.arr[indx];
  return returnVal;
}

void set(struct CA arr, int indx, int value) {
  int oldEl = arr.arr[indx];
  if (!(indx > arr.used)) {
    arr.arr[indx] = value;
    printf("Old value: %d\nNew value: %d\n", oldEl, value);
  } else {
    printf("That's not allowed here, yo.\n");
  }
}

struct CA resize(struct CA arr) {
  struct CA newArr;
  newArr.arr = malloc(sizeof(arr.arr) * 2);
  newArr.capacity = sizeof(newArr.arr);
  newArr.used = arr.used;
  for (int i = 0; i < arr.used; i++) {
    newArr.arr[i] = arr.arr[i];
  }
  printf("Resized array. Size is now %d\n", newArr.capacity);
  free(arr.arr);
  return newArr;
}

struct CA append(struct CA arr, int value) {
  printf("U: %d  C: %d\n", arr.used, arr.capacity);
  if (arr.used == arr.capacity) {
    arr = resize(arr);
  }
  arr.arr[arr.used] = value;
  arr.used += 1;
  printf("Done.\n");
  return arr;
}

struct CA trim_to_size(struct CA arr) {
  struct CA newArr;
  newArr.capacity = arr.used;
  newArr.used = arr.used;
  newArr.arr = malloc(sizeof(arr.used));
  for (int i = 0; i < arr.used; i++) {
    newArr.arr[i] = arr.arr[i];
  }
  printf("Trimmed array to %d.\n", newArr.used);
  return newArr;
}
